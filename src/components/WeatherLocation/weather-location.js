import React, { Component } from 'react'
import Location from './location'
import WeatherData from './WeatherData/weather-data'
import PropTypes from 'prop-types'
import './styles.css'
import data from '../../data.json'

class WeatherLocation extends Component {
  constructor(){
    super()
    this.state = {
      city: "Culiacán, Sinaloa, Mex.",
      data: data.weather,
    }
  }
  handleUpdateClick = () => {
    this.setState({
      city: "La Plama, Navolato, Sin.",
      data: data.weather_2,
    })
  }
  render(){
    return(
      <div className="weatherLocation">
        <Location city={this.state.city}/>
        <WeatherData data={this.state.data} />
        <button onClick={this.handleUpdateClick}>Actualizar</button>
      </div>
    )
  }
}


export default WeatherLocation