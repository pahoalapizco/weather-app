import React from 'react'
import WeatherTemperature from './weather-temperature'
import WeatherExtraInfo from './weather-extra-info'
import PropTypes from 'prop-types'
import './styles.css'

const WeatherData = (props) => {
	const {
		temperature,
		weatherState,
		humidity,
		wind,
	} = props.data

	return(
		<div className="WeatherData">
			<WeatherTemperature 
				temperature={temperature} 
				weatherState={weatherState}
			/>
			<WeatherExtraInfo 
				humidity={humidity} 
				wind={wind} 
			/>
		</div>
	)
}

WeatherData.propTypes = {
	data: PropTypes.shape( { //Esperamos un objeto con determinada forma...
		temperature: PropTypes.number.isRequired,
		weatherState: PropTypes.string.isRequired,
		humidity: PropTypes.number.isRequired,
		wind: PropTypes.string.isRequired,
	}),
}

export default WeatherData