import React from 'react'
import WeatherIcon from 'react-icons-weather'
import PropTypes from 'prop-types'
import {
	CLEAR,
	CLOUD,
	CLOUDY,
	RAIN, 
	SNOW,
} from '../../../constants/weather'
import './styles.css'
//Diccionario de iconos:
const Icons = {
  [CLEAR]:   "800",
  [CLOUD]:   "802",
  [CLOUDY]:  "804",
  [RAIN]:    "531",
  [SNOW]:    "622",
}

const getWeatherIcon = (weatherState) => {
  const icon = Icons[weatherState]
  const sizeIcon = "4x"

  if(icon){
    return (
      <WeatherIcon  className="wicon"
          name="owm"  iconId={icon}
          flip="horizontal" rotate="90" 
        />
    )
  } else {
    return (
      <WeatherIcon   className="wicon"  name="owm" 
          iconId="800" flip="horizontal"   rotate="90" 
        />
    )
  }
}
const WeatherTemperature = (props) => {
  const {
    temperature,
    weatherState
  } = props

  return(
    <div className="WeatherTemperature">
      {
        getWeatherIcon(weatherState)
      }
      <span className="temperature">{temperature}</span>
      <span className="temperatureType">°C </span>
    </div>
  )
}

WeatherTemperature.propTypes = {
	temperature: PropTypes.number.isRequired,
	weatherState: PropTypes.string,
}

export default WeatherTemperature