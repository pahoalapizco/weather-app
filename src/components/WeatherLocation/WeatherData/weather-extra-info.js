import React from 'react';
import PropTypes from 'prop-types'
import './styles.css'

const WeatherExtraInfo =(props) => {
  const {
    humidity,
    wind
  } = props
  return (
    <div className="WeatherExtraInfo">
      <span className="extraInfoText"> {`Humedad: ${humidity} % h`}</span>
      <span className="extraInfoText"> {`Vientos: ${wind}`} </span>
    </div>
  )
}

WeatherExtraInfo.propTypes = {
  humidity: PropTypes.number.isRequired,
  wind: PropTypes.string.isRequired,
}

export default WeatherExtraInfo